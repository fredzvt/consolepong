﻿using System;

namespace ConsolePong
{
    public class Ball
    {
        public int Top { get; private set; }
        public int Left { get; set; }

        public int XVelocity { get; set; }
        public int YVelocity { get; set; }

        private int lastDrawnTop;
        private int lastDrawnLeft;
        private char lastDrawnChar;
        private int middleLinePosition;

        private int bottomLimit;
        private int topLimit;
        private Random rnd;

        public Ball()
        {
            rnd = new Random();
            bottomLimit = Console.WindowHeight - 3;
            topLimit = 5;
            middleLinePosition = Console.WindowWidth / 2;
            SetRandomInitialPositionAndVelocity(true);
            lastDrawnTop = Top;
            lastDrawnLeft = Left;
        }

        public void SetRandomInitialPositionAndVelocity(bool startToRight)
        {
            Top = rnd.Next(topLimit + 5, bottomLimit - 5);
            Left = middleLinePosition;

            YVelocity = 1;

            if (startToRight)
                XVelocity = 1;
            else
                XVelocity = -1;
        }

        public void Draw()
        {
            Erase();

            Console.BackgroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(Left, Top);
            Console.Write(" ");

            lastDrawnTop = Top;
            lastDrawnLeft = Left;
        }

        public void Erase()
        {
            Console.SetCursorPosition(lastDrawnLeft, lastDrawnTop);

            if (lastDrawnLeft == middleLinePosition)
            {
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(":");
            }
            else
            {
                Console.ResetColor();
                Console.Write(" ");
            }
        }

        public void BounceOnPaddle()
        {
            XVelocity = -XVelocity;
            Program.playBeep = true;
        }

        public void UpdatePosition()
        {
            Top += YVelocity;
            Left += XVelocity;

            if (Top < topLimit)
            {
                Top = topLimit;
                YVelocity = -YVelocity;
                Program.playBeep = true;
            }
            else if (Top > bottomLimit)
            {
                Top = bottomLimit;
                YVelocity = -YVelocity;
                Program.playBeep = true;
            }
        }
    }
}
