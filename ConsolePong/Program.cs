﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsolePong
{
    public enum Input
    {
        None, Up, Down
    }

    public enum States
    {
        GameInit,
        Game,
        Goal
    }

    public class Program
    {
        static Paddle P1Paddle;
        static Paddle P2Paddle;
        static Ball Ball;

        static States State;

        static int P1Score = 0;
        static int P2Score = 0;
        static bool startToRight = true;

        static void Main(string[] args)
        {
            // First, set console properties.
            SetupConsole();

            // Initialize state machine.
            State = States.GameInit;

            // Initialize game objects.
            Ball = new Ball();
            P1Paddle = new Paddle(3);
            P2Paddle = new Paddle(Console.WindowWidth - 4);

            // Start first thread to play sounds.
            var soundThread = new Thread(PlaySound);
            soundThread.Start();

            // Start second thread to play sounds.
            var sfxThread = new Thread(PlaySoundEffect);
            sfxThread.Start();

            // Draw interface
            DrawCreditsAndTitle();
            DrawBorders();
            DrawScore();

            // Main loop.
            while (true)
            {
                // Let paddle be handled by the players all the times.
                UpdatePaddlePositions();

                // Execute a different logic for each game state.
                switch (State)
                {
                    case States.GameInit:
                        GameInitLoop();
                        break;

                    case States.Game:
                        GameLoop();
                        break;

                    case States.Goal:
                        GoalLoop();
                        break;

                    default:
                        throw new Exception("Unexpected state.");
                }

                // Slow down the main loop a little.
                Thread.Sleep(25);
            }
        }

        public static void SetupConsole()
        {
            // Set windows and buffer size.
            Console.SetBufferSize(90, 40);
            Console.SetWindowSize(90, 40);

            // Hide cursor.
            Console.CursorVisible = false;
        }
        public static void DrawCreditsAndTitle()
        {
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Green;

            Console.SetCursorPosition(1, 1);
            Console.Write("PG03 - Frederico Zveiter");

            Console.SetCursorPosition(Console.WindowWidth - 24, 1);
            Console.Write("Prog 1 Final Assignment");

            Console.SetCursorPosition(Console.WindowWidth - 6, 2);
            Console.Write("Pong!");
        }
        public static void DrawScore()
        {
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Yellow;

            var middle = Console.WindowWidth / 2;

            Console.SetCursorPosition(middle - 15, 1);
            Console.Write("(W/S) Player 1 : Player 2 (Arrows)");

            var p1ScoreAsString = P1Score.ToString();
            Console.SetCursorPosition(middle - p1ScoreAsString.Length - 1, 2);
            Console.Write("{0} : {1}", p1ScoreAsString, P2Score);

            Console.SetCursorPosition(middle, 3);
            Console.Write(":");
        }
        public static void DrawBorders()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            for (var i = 4; i < Console.BufferHeight - 1; i++)
            {
                Console.SetCursorPosition(Console.WindowWidth / 2, i);
                Console.Write(":");
            }

            Console.BackgroundColor = ConsoleColor.White;
            var verticalBorders = "";

            for (var i = 1; i < Console.BufferWidth - 1; i++)
            {
                verticalBorders += " ";
            }

            Console.SetCursorPosition(1, 4);
            Console.Write(verticalBorders);

            Console.SetCursorPosition(1, Console.BufferHeight - 2);
            Console.Write(verticalBorders);
        }

        public static Input GetPlayer1Input()
        {
            if (Keyboard.IsKeyDown(KeyCode.W))
                return Input.Up;

            else if (Keyboard.IsKeyDown(KeyCode.S))
                return Input.Down;

            return Input.None;
        }
        public static Input GetPlayer2Input()
        {
            if (Keyboard.IsKeyDown(KeyCode.Up))
                return Input.Up;

            else if (Keyboard.IsKeyDown(KeyCode.Down))
                return Input.Down;

            return Input.None;
        }

        public static void GameLoop()
        {
            Ball.UpdatePosition();
            CheckBallPaddleCollisions();
            ChackBallGoalCollisions();
            Ball.Draw();
        }
        public static void GoalLoop()
        {
            BlinkBall();

            if (blinkOver)
            {
                Ball.SetRandomInitialPositionAndVelocity(startToRight);

                blinkOver = false;
                blinkStart = DateTime.Now;
                State = States.GameInit;
            }
        }
        public static void GameInitLoop()
        {
            BlinkBall();

            if (blinkOver)
            {
                blinkOver = false;
                blinkStart = DateTime.Now;
                State = States.Game;
            }
        }

        public static void CheckBallPaddleCollisions()
        {
            if (Ball.Left >= Console.BufferWidth - 5 && Ball.XVelocity > 0)
            {
                if (!(Ball.Top < P2Paddle.Top || Ball.Top > P2Paddle.Top + P2Paddle.Size))
                {
                    Ball.BounceOnPaddle();
                }
            }
            else if (Ball.Left <= 4 && Ball.XVelocity < 0)
            {
                if (!(Ball.Top < P1Paddle.Top || Ball.Top > P1Paddle.Top + P1Paddle.Size))
                {
                    Ball.BounceOnPaddle();
                }
            }
        }
        public static void ChackBallGoalCollisions()
        {
            if (Ball.Left == 1 || Ball.Left == Console.BufferWidth - 2)
            {
                if (Ball.Left == 1)
                {
                    P2Score++;
                    startToRight = true;
                }
                else
                {
                    P1Score++;
                    startToRight = false;
                }

                playSfx = true;
                DrawScore();

                blinkOver = false;
                blinkStart = DateTime.Now;
                State = States.Goal;
            }
        }
        public static void UpdatePaddlePositions()
        {
            P1Paddle.UpdatePosition(GetPlayer1Input());
            P1Paddle.Draw();

            P2Paddle.UpdatePosition(GetPlayer2Input());
            P2Paddle.Draw();
        }

        public static bool blinkOver = false;
        public static DateTime blinkStart = DateTime.Now;
        public static DateTime blinkLastTime = DateTime.Now;
        public static bool blinkFlag = true;
        public static void BlinkBall()
        {
            if (DateTime.Now.Subtract(blinkStart).TotalSeconds >= 1)
            {
                blinkOver = true;
            }
            else
            {
                if (DateTime.Now.Subtract(blinkLastTime).TotalMilliseconds > 300)
                {
                    blinkLastTime = DateTime.Now;
                    blinkFlag = !blinkFlag;

                    if (blinkFlag)
                    {
                        Ball.Erase();
                    }
                    else
                    {
                        Ball.Draw();
                    }
                }
            }
        }

        public static bool playBeep = false;
        public static void PlaySound()
        {
            while (true)
            {
                if (playBeep)
                {
                    playBeep = false;
                    Console.Beep(500, 100);
                }

                Thread.Sleep(10);
            }
        }

        public static bool playSfx = false;
        public static void PlaySoundEffect()
        {
            while (true)
            {
                if (playSfx)
                {
                    playSfx = false;
                    Console.Beep(700, 500);
                }

                // Slow the loop a little.
                Thread.Sleep(10);
            }
        }
    }
}
