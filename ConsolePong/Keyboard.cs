﻿using System;

namespace ConsolePong
{
    /*
     * Adapted from: http://stackoverflow.com/questions/4351258/c-sharp-arrow-key-input-for-a-console-app
    */

    public enum KeyCode : int
    {
        Up = 0x26, Down = 0x28,     // Virtual Key Code: http://msdn.microsoft.com/en-us/library/dd375731(v=vs.85).aspx
        W = 87, S = 83              // ASCII
    }

    public static class Keyboard
    {
        /// <summary>
        /// A positional bit flag indicating the part of a key state denoting
        /// key pressed.
        /// </summary>
        private const int KeyPressed = 0x8000;

        /// <summary>
        /// Returns a value indicating if a given key is pressed.
        /// </summary>
        /// <param name="key">The key to check.</param>
        /// <returns>
        /// <c>true</c> if the key is pressed, otherwise <c>false</c>.
        /// </returns>
        public static bool IsKeyDown(KeyCode key)
        {
            return (GetKeyState((int)key) & KeyPressed) != 0;
        }

        /// <summary>
        /// Gets the key state of a key.
        /// </summary>
        /// <param name="key">Virtuak-key code for key.</param>
        /// <returns>The state of the key.</returns>
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern short GetKeyState(int key);
    }
}
