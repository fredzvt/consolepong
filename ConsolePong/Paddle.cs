﻿using System;

namespace ConsolePong
{
    public class Paddle
    {
        public int Top { get; private set; }
        public int Left { get; set; }
        public int Size { get; private set; }
        public int Velocity { get; set; }

        private int lastDrawnTop;
        private int bottomLimit;
        private int topLimit;

        public Paddle(int left)
        {
            Size = 5;
            Top = (Console.WindowHeight / 2) - (Size / 2);
            Left = left;
            Velocity = 1;

            lastDrawnTop = Top;
            bottomLimit = Console.WindowHeight - Size - 2;
            topLimit = 5;
        }

        public void Draw()
        {
            Erase();

            Console.BackgroundColor = ConsoleColor.Yellow;
            for (var line = 0; line < Size; line++)
            {
                Console.SetCursorPosition(Left, Top + line);
                Console.Write(" ");
            }

            lastDrawnTop = Top;
        }

        public void Erase()
        {
            Console.ResetColor();
            for (var line = 0; line < Size; line++)
            {
                Console.SetCursorPosition(Left, lastDrawnTop + line);
                Console.Write(" ");
            }
        }

        public void UpdatePosition(Input input)
        {
            switch (input)
            {
                case Input.Up: 
                    
                    Top -= Velocity;

                    if (Top < topLimit)
                        Top = topLimit;

                    break;

                case Input.Down:

                    Top += Velocity;

                    if (Top > bottomLimit)
                        Top = bottomLimit;

                    break;
            }
        }
    }
}
